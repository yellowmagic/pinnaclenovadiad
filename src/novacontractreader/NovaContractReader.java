/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package novacontractreader;

import javax.swing.ImageIcon;
import javax.swing.UIManager;

/**
 *
 * @author simsa
 */
public class NovaContractReader {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        setLookAndFeel();
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//Here I'm loading whatever I want to have show up first, I'm keeping the rest of this class clean so that I can launch other forms from it
                Main m = new Main();
                m.setTitle("Nova Contract Reader");
//                m.setIconImage(new ImageIcon(getClass().getResource("icon_56x56.png")).getImage());
                m.setVisible(true);


            }
        });
    }

    public static void setLookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.out.println("Error setting native LAF: " + e);
        }
    }
}
